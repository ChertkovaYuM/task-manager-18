# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Yuliya Chertkova

* **E-MAIL**: ychertkova@t1-consulting.com

* **E-MAIL**: t1-consulting@t1-consulting.com

## SOFTWARE

* OpenJDK 8

* Intellij IDEA

* MS Windows 10

## HARDWARE

* **RAM**: 8Gb

* **CPU**: i5

* **HDD**: 256Gb

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
