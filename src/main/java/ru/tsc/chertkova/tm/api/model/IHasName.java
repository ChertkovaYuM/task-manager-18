package ru.tsc.chertkova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
