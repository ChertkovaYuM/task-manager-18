package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
