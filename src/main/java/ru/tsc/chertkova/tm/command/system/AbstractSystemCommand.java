package ru.tsc.chertkova.tm.command.system;

import ru.tsc.chertkova.tm.api.service.ICommandService;
import ru.tsc.chertkova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public String getArgument() {
        return null;
    }

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
