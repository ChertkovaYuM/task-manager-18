package ru.tsc.chertkova.tm.exception.system;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
