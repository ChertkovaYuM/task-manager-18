package ru.tsc.chertkova.tm.enumerated;

public enum RoleType {

    USUAL("Usual user"),

    ADMIN("Administrator"),

    TEST("Test");

    private final String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
